const axios = require("axios");
const fs = require("fs");
const yaml = require("yamljs");

let config, path;

// Step #1
// Gathering the Configuration

path = process.argv[process.argv.length - 1];

config = fs.readFileSync(path, "utf8");
config = yaml.parse(config);

// Step #2
// Stabilize the Configuration

if (config["input"]["kind"] === "regexp") {
    config["input"]["parseField"] = config["input"]["parseField"].split(",").map(item => item.trim());
}

// Step #3
// Initialize the Configuration

const Watcher = require("./src/class/watcher.class");

const watcherObject = new Watcher(config["input"]);

watcherObject.on("data", async function (parsedObject) {

    try {

        await axios.post(config["output"]["path"], parsedObject);

    } catch (E) {

        console.error(E.message);

    }

});

// Step #4
// Reboot after 60 minutes

setTimeout(function () {

    process.exit(0);

}, 60 * 60 * 1000);