const {exec} = require("child_process");

const {EventEmitter} = require("events");

class Watcher extends EventEmitter {

    /**
     * @param opts
     */
    constructor(opts) {

        super();

        const that = this;

        const tail = exec(`tail -n 0 -f ${opts["file"]}`);

        tail["stdout"].on("data", function (data) {

            const lines = data.trim().split("\n");

            lines.forEach(lineObject => {

                console.log(lineObject);

                if (opts["kind"] === "regexp") {

                    let parsedObject = that.parseLine(opts["parseFormat"], lineObject);

                    if (parsedObject === null)

                        return false;

                    parsedObject = that.matchFields(opts["parseField"], parsedObject);

                    if (parsedObject === false)

                        return false;

                    that.emit("data", parsedObject);

                } else if (opts["kind"] === "json") {

                    try {

                        that.emit("data", JSON.parse(lineObject));

                    } catch (E) {

                        console.log("Error while parsing JSON.", {
                            error: E.message,
                            lineObject: lineObject
                        });

                        return false;

                    }

                }

            });

        });

        tail["stderr"].on("data", function (data) {

            throw new Error(data);

        });

    }

    /**
     * parseLine()
     * @param {String} formatObject
     * @param {String} lineObject
     * @returns {*}
     */
    parseLine(formatObject, lineObject) {

        // Step #1
        // Handling the request

        const regExpObject = new RegExp(formatObject);

        let resultObject = regExpObject.exec(lineObject.trim());

        if (resultObject === null) {

            console.error("Error while formatting.", {
                format: formatObject,
                line: lineObject
            });

            return null;

        }

        // Step #2
        // Finalize and return

        resultObject.shift();

        for (let X in resultObject)

            if (Number(resultObject[X]) == resultObject[X])

                resultObject[X] = Number(resultObject[X]);

        return resultObject;

    }

    /**
     * matchFields()
     * @param {Array} listOfFields
     * @param {Array} listOfParts
     * @returns {Boolean|Object}
     */
    matchFields(listOfFields, listOfParts) {

        // Step #1
        // Make sure the number of elements in two
        // - inputs are matched.

        if (listOfFields.length !== listOfParts.length) {

            console.error("Error while matching.", {
                listOfFields: listOfFields,
                listOfParts: listOfParts
            });

            return false;

        }

        // Step #2
        // Handling the request.

        let resultObject = {};

        for (let X = 0; X < listOfParts.length; X++)

            resultObject[listOfFields[X]] = listOfParts[X];

        // Step #3
        // Finalize and return.

        return resultObject;

    }

}

module.exports = Watcher;